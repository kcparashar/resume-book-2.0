from django.conf.urls import patterns, url, include

from searchapp import views

urlpatterns = patterns('',
    url(r'^$', include('haystack.urls')),
    url(r'^browse/',  views.listview, name='listview'),
)
