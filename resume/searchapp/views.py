from django.shortcuts import render

from submitapp.models import Student

def listview(request):
    students = Student.objects.all()
    return render(request, 'searchapp/listview.html', {'students': students})
