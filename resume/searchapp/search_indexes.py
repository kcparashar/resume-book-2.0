from submitapp.models import Student
from haystack import indexes

class StudentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Student

    def prepare(self, obj):
        data = super(FileIndex, self
