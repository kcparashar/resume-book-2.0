from django.shortcuts import render
from django.http import HttpResponse

from .forms import StudentForm

def submit(request):
    if request.method == 'POST':
        form = StudentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponse('Thank you for submitting your resume.')
    else:
        form = StudentForm()
    return render(request, 'submitapp/submit.html', {'form': form})

