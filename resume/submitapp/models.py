import os
from django.db import models

class Semester(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class AreaOfInterest(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

def _get_upload_path_(instance, filename):
    return os.path.join('resumes',
                        instance.graduation_semester.name,
                        instance.athena_username)

class Student(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    athena_username = models.CharField(max_length=200)
    graduation_semester = models.ForeignKey('Semester')
    resume = models.FileField(upload_to=_get_upload_path_)
    want_internship = models.BooleanField()
    want_full_time = models.BooleanField()
    want_part_time = models.BooleanField()
    areas_of_interest = models.ManyToManyField('AreaOfInterest')
    gpa = models.IntegerField()
    hkn_member = models.BooleanField()
    
    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)        

