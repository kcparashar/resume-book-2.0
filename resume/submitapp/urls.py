from django.conf.urls import patterns, url

from submitapp import views

urlpatterns = patterns('',
    url(r'^$', views.submit, name='submit'),
)
