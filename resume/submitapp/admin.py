from django.contrib import admin
from .models import Semester, AreaOfInterest, Student

admin.site.register(Semester)
admin.site.register(AreaOfInterest)
admin.site.register(Student)
